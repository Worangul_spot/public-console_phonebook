﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApplication2
{
    class Contact
    {
        public string Name { get; set; }
        public string Number { get; set; }
        
    }
    

    class Program
    {
        static void Main(string[] args)
        {
            var dane = new List<Contact>()
            {
                new Contact() {Name = "Tomasz", Number = "1235465"},
                new Contact() {Name = "Arkadiusz", Number = "134534523"},
                new Contact() {Name = "Michał", Number = "124353453"},
                new Contact() {Name = "Maciej", Number = "3425"},
            };
            var options = new List<string> { "1-SHOW ALL Contacts.", "2-SEARCH by Name.", "3-SEARCH by Number.", "4-ADD contact.", "5-REMOVE contact", "6-EXIT" };
            char choose;
        start:
            Console.WriteLine("Welcome in Console Phonebook. Write integer number and press");
            Console.WriteLine("enter for choose option, from list below:");
            foreach (string element in options)
            {
                Console.WriteLine(element);
            };
            choose = char.Parse(Console.ReadLine());

            switch (choose)
            {
                case '1':
                    foreach(Contact k in dane)
                    {
                        Console.WriteLine(k.Name+" "+ k.Number);
                    }
                    goto start;
                case '2':
                    Console.WriteLine("Input Name of desired Contact now, then press enter: ");
                    var doZnalezienia = Console.ReadLine();
                    Contact znaleziony = dane.Find(c => c.Name == doZnalezienia);
                    if (znaleziony != null)
                    {
                        Console.WriteLine("Found phone number with name: " + znaleziony.Name + " is " + znaleziony.Number);
                        Console.ReadLine();
                    }
                    else
                    { Console.WriteLine("There is no such number"); };
                    goto start;
                case '3':
                    var doZnalezienia2 = Console.ReadLine();
                    Contact znaleziony2 = dane.Find(c => c.Number == doZnalezienia2);
                    if (znaleziony2 !=null)
                    {
                        Console.WriteLine("Found phone number "+ znaleziony2.Number +" is belong to " + znaleziony2.Name);
                    }
                    else
                    {
                        Console.WriteLine("There is no such number.");
                    }
                    goto start;        
                case '4':
                    Console.WriteLine("First, input Name of your new contact: ");
                    string newName = Console.ReadLine();
                    Console.WriteLine("... now, input number of your new contact (input is string, so you can technically type letters as well)");
                    string newNumber = Console.ReadLine();
                    dane.Add(new Contact() { Name = newName, Number = newNumber });
                    Console.WriteLine("Your Contact is succefully added!");
                    Console.WriteLine("");
                    goto start;
                case '5':
                    Console.WriteLine("Point at contact, you want to remove, by write its name below:");
                    var doZnalezienia3 = Console.ReadLine();
                    Contact usuwCont = dane.Find(c => c.Name == doZnalezienia3);
                    dane.Remove(usuwCont);
                    if(usuwCont != null)
                    {
                        Console.WriteLine();
                        Console.WriteLine("Demanded contat was found and removed.");
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine("There is NO such file. Try maybe try other name.");
                        Console.WriteLine();
                    }
                    goto start;
                case '6':
                    Environment.Exit(1);
                    break;
                default:
                    Console.WriteLine("This is not an option. Try again.");
                    goto start;
            }

        }
    }
}

